FROM amazonlinux

MAINTAINER Thorben Stangenberg <thorben@stangenberg.ch>

## Do what is needed

## set an environent variable
ENV HOME /app

## add files
#ADD src dest

## install general development tools
RUN yum install -y gcc-c++ make less

## install AWS Cli
RUN yum install -y groff aws-cli

## install Java
RUN yum install -y java-1.8.0-openjdk.x86_64

## install nodejs
RUN curl --silent --location https://rpm.nodesource.com/setup_8.x | bash - && \
    yum install -y nodejs

## install yarn
RUN curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo && \
    yum install -y yarn

## install serverless
RUN yarn global add serverless    

## check installations
RUN node -v
RUN npm -v
RUN yarn -v
RUN aws --version

## cleanup
RUN yum clean all && \
    rm -rf /var/cache/yum