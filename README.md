# Node.js
docker container with nodejs environment

Image: [stangenberg/nodejs][gitlab]

## Features
- [AWS CLI](https://aws.amazon.com/cli) 
- [Node.js](https://nodejs.org) 
- [Yarn](https://yarnpkg.com)
- [OpenJDK](http://openjdk.java.net)

## Exposed volumes
None.

## Exposed ports
None

## Environment Variables
None.

## Usage

### Show node help
```bash
docker run stangenberg/nodejs:latest node --help
```

### See Java Version
```bash
docker run stangenberg/nodejs:latest java -version
```

### Install [Serverless](https://serverless.com) Framework
```bash
docker run -e https_proxy -e http_proxy -v "$(pwd)":/app -w /app --user $(id -u):$(id -g) stangenberg/nodejs:latest yarn add serverless --dev
```

## Build
Make is used as build system.
- `make` / starts normal docker build.
- `make run` / build and run the container. This uses `imagename` as container name and automatically stops a running container with an equal name beforehand.
- `make bash` /  build, run the container and start a prompt.
- `make ncbuild` / normal build without using the docker cache ( --no-cache ).

[Docker Build Reference https://docs.docker.com/reference/builder/](https://docs.docker.com/reference/builder/)

## License
[Published under the MIT License][LICENSE]

[gitlab]: https://gitlab.com/stangenberg/docker-nodejs
[license]: https://gitlab.com/stangenberg/docker-nodejs/blob/master/LICENSE.md
